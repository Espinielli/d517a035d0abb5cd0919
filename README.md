Visualization of European FIRs (those starting w/ 'E', 'L' or 'UK', 'UG', 'UD', 'BK', 'GC'.

Click on FIR to zoom in/out.

Geometries from the [Network Manager](https://www.eurocontrol.int/network-manager) and available in [this repo](https://github.com/espinielli/firs).

Built with [blockbuilder.org](http://blockbuilder.org)